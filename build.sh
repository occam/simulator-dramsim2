#!/bin/bash

cd package
make

cd traces
python2 traceParse.py k6_aoe_02_short.trc.gz
python2 traceParse.py mase_art.trc.gz
cd ..

cd ..

# Install
mv package/DRAMSim ../build/.
mkdir -p ../build/traces
mv package/traces/*.trc ../build/traces/.
cp -r package/ini ../build/.
cp package/system.ini.example ../build/.
