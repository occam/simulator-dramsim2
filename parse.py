# Generates an output.json representing the output of the simulator.

import re
import json
from occam import Occam

object = Occam.load()

f = open(object.output(), 'r')

class Results:
    warnings = []
    errors   = []

    data     = None

    def hash(self):
        return {
            "data":     self.data,
            "warnings": self.warnings,
            "errors":   self.errors
        }

results = Results()

class LatencyEntry:
    min    = 0
    max    = 0
    amount = 0

    def hash(self):
        return {
            "min": self.min,
            "max": self.max,
            "amount": self.amount
        }

class Bank:
    id         = 0
    bandwidth  = 0.0
    latency    = 0.0

    def hash(self):
        return {
            "id": self.id,
            "bandwidth": self.bandwidth,
            "latency": self.latency
        }

class Rank:
    id                = 0
    reads             = 0
    read_amt          = 0
    writes            = 0
    write_amt         = 0
    banks             = []
    avg_power         = 0.0
    avg_bg_power      = 0.0
    avg_pre_power     = 0.0
    avg_burst_power   = 0.0
    avg_refresh_power = 0.0
    latency           = []

    def hash(self):
        return {
            "id": self.id,
            "reads": self.reads,
            "readsSize": self.read_amt,
            "writes": self.writes,
            "writesSize": self.write_amt,
            "averagePower": self.avg_power,
            "averageBackgroundPower": self.avg_bg_power,
            "averageActPrePower": self.avg_pre_power,
            "averageBurstPower": self.avg_burst_power,
            "averageRefreshPower": self.avg_refresh_power,
            "latency": [latencyEntry.hash() for latencyEntry in self.latency],
            "banks": [bank.hash() for bank in self.banks]
        }

class Channel:
    id          = 0
    returns     = 0
    returns_amt = 0
    bandwidth   = 0.0
    pending     = 0
    pending_amt = 0
    ranks       = []

    def hash(self):
        return {
            "id": self.id,
            "returnTransactions": self.returns,
            "returnTransactionsSize": self.returns_amt,
            "bandwidth": self.bandwidth,
            "pendingTransactions": self.pending,
            "pendingTransactionsSize": self.pending_amt,
            "ranks": [rank.hash() for rank in self.ranks]
        }

    def __str__(self):
        return "Channel #" + str(self.id) +\
          "\n  Return Tx: " + str(self.returns) +\
          "\n  Amount: " + str(self.returns_amt) +\
          "\n  AvgBandwidth: " + str(self.bandwidth) + "GB/s"

channels = []
channel = None

power_rank   = None
rank         = None
bank         = None
latencyEntry = None

for line in f:
    line = line.strip()
    # FILLER TO IGNORE
    if line.startswith("----"):
        continue

    elif line.startswith("====="):
        continue

    # WARNINGS
    elif line.startswith("WARNING:"):
        m = re.match(r"^WARNING:\s+(.+)$", line)

        if m == None:
            continue

        results.warnings.append({"message": m.group(1)})

    # CHANNEL
    elif line.startswith("==== "):
        m = re.match(r"^==== (.+) ====$", line)
        if m == None:
            continue

        header = m.group(1)
        m = re.match(r"^Channel \[(\d+)\]$", header)

        if m == None:
            continue

        if channel != None:
            if rank != None:
                channel.ranks.append(rank)

            channels.append(channel)

        rank = None
        bank = None
        latencyEntry = None
        channel = Channel()
        channel.id = int(m.group(1))

    elif line.startswith("== Pending Transactions"):
        m = re.match(r"^== Pending Transactions : (\d+)\s\((\d+)\)==$", line)

        if m == None:
            continue

        if channel != None:
            channel.pending = int(m.group(1))
            channel.pending_amt = int(m.group(2))

    elif line.startswith("Total Return Transactions"):
        m = re.match(r"^Total Return Transactions : (\d+) \((\d+) bytes\).+\s([\d.]+)GB/s$", line)

        if m == None:
            continue

        if channel != None:
            channel.returns     = int(m.group(1))
            channel.returns_amt = int(m.group(2))
            channel.bandwidth   = float(m.group(3))

    # RANK
    elif line.startswith("-Rank "):
        m = re.match(r"^-Rank\s+(\d+)", line)

        if m == None:
            continue

        if channel != None and rank != None:
            channel.ranks.append(rank)

        rank = Rank()
        rank.id = int(m.group(1))
    elif line.startswith("-Reads "):
        m = re.match(r"^-Reads\s+:\s+(\d+)\s\((\d+) bytes\)$", line)

        if m == None:
            continue

        if rank != None:
            rank.reads     = int(m.group(1))
            rank.read_amt = int(m.group(2))

    elif line.startswith("-Writes "):
        m = re.match(r"^-Writes\s+:\s+(\d+)\s\((\d+) bytes\)$", line)

        if m == None:
            continue

        if rank != None:
            rank.writes     = int(m.group(1))
            rank.write_amt = int(m.group(2))

    # BANK
    elif line.startswith("-Bandwidth "):
        m = re.match(r"^-Bandwidth\s+/\s+Latency\s+\(Bank (\d+)\):\s+([\d.]+)\sGB/s\s+([\d.]+)\sns$", line)

        if m == None:
            continue

        bank = Bank()
        bank.id        = m.group(1)
        bank.bandwidth = m.group(2)
        bank.latency   = m.group(3)

        if rank != None:
            rank.banks.append(bank)

    # RANK POWER DATA
    elif line.startswith("== Power Data for"):
        m = re.match(r"^== Power Data for Rank\s+(\d+)$", line)

        if m == None:
            continue

        if channel != None:
            if rank != None:
                channel.ranks.append(rank)
                rank = None
            power_rank = channel.ranks[int(m.group(1))]

    elif line.startswith("Average Power"):
        m = re.match(r"^Average Power \(watts\)\s+:\s+([\d.]+)$", line)

        if m == None:
            continue

        if power_rank != None:
            power_rank.avg_power = float(m.group(1))

    elif line.startswith("-Background (watts)"):
        m = re.match(r"^-Background \(watts\)\s+:\s+([\d.]+)$", line)

        if m == None:
            continue

        if power_rank != None:
            power_rank.avg_bg_power = float(m.group(1))

    elif line.startswith("-Act/Pre "):
        m = re.match(r"^-Act/Pre\s+\(watts\)\s+:\s+([\d.]+)$", line)

        if m == None:
            continue

        if power_rank != None:
            power_rank.avg_pre_power = float(m.group(1))

    elif line.startswith("-Act/Pre "):
        m = re.match(r"^-Act/Pre\s+\(watts\)\s+:\s+([\d.]+)$", line)

        if m == None:
            continue

        if power_rank != None:
            power_rank.avg_pre_power = float(m.group(1))

    elif line.startswith("-Burst "):
        m = re.match(r"^-Burst\s+\(watts\)\s+:\s+([\d.]+)$", line)

        if m == None:
            continue

        if power_rank != None:
            power_rank.avg_burst_power = float(m.group(1))

    elif line.startswith("-Refresh "):
        m = re.match(r"^-Refresh\s+\(watts\)\s+:\s+([\d.]+)$", line)

        if m == None:
            continue

        if power_rank != None:
            power_rank.avg_refresh_power = float(m.group(1))

    # LATENCY
    elif line.startswith("---  Latency list"):
        m = re.match(r"^---\s+Latency\slist\s\((\d+)\)$", line)

        if m == None:
            continue

    elif line.startswith("["):
        m = re.match(r"^\[(\d+)-(\d+)\]\s+:\s+(\d+)$", line)

        if m == None:
            continue

        latencyEntry        = LatencyEntry()
        latencyEntry.min    = int(m.group(1))
        latencyEntry.max    = int(m.group(2))
        latencyEntry.amount = int(m.group(3))

        if power_rank != None:
            power_rank.latency.append(latencyEntry)

        latencyEntry = None
    else:
        continue

if channel != None:
    channels.append(channel)

results.data = {"channels": [channel.hash() for channel in channels]}

o = open("%s/output.json" % (object.path()), "w+")

o.write(json.dumps(results.hash()["data"]))

o.close()

# Report warnings/errors to OCCAM
Occam.finish(results.hash()["warnings"], results.hash()["errors"])
