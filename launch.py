import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path   = os.path.dirname(__file__)
simulator_path = "%s/package" % (scripts_path)
traces_path    = "%s/traces"  % (simulator_path)
binary         = "%s/DRAMSim" % (simulator_path)
job_path       = os.getcwd()

object = Occam.load()

# Open object.json for command line options
data = object.configuration("General Options")

# Generate input
input_file = open('system.ini', 'w+')

for k, v in data.items():
  if isinstance(v, dict):
    for k,v in v.items():
      input_file.write(k.upper()+"="+str(v)+"\n")
  if isinstance(v, list):
    # Handle size units
    if v[1] == "GB":
      v[0] = v[0] * 1024
    input_file.write(k.upper()+"="+str(v[0])+"\n")
  else:
    input_file.write(k.upper()+"="+str(v)+"\n")

ddr_data = object.configuration("DRAM Options")

# Generate input
input_file = open('device.ini', 'w+')

for k, v in ddr_data.items():
  if isinstance(v, dict):
    for k,v in v.items():
      input_file.write(k+"="+str(v)+"\n")
  else:
    input_file.write(k+"="+str(v)+"\n")

input_file.close()

trace_path = "%s/%s.trc" % (traces_path, (data["trace"] or "k6_aoe_02_short"))

# Form arguments
args = [binary,
        "-t", trace_path,
        "-s", "system.ini",
        "-S", str(data["memory_size"] or 2048),
        "-d", "device.ini",
        "-c", str(data["cycle_count"] or 10000)]

# Form command line
command = ' '.join(args)

# Form command to gather results
finish_command = "python %s/parse.py" % (scripts_path)

# Tell OCCAM how to run DRAMSim2
Occam.report(command, finish_command)
